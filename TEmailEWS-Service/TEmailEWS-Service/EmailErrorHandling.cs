﻿using System;
using System.Collections;
using System.Threading.Tasks;

/// <summary>
/// This tracks email error counts with a hash table
/// </summary>
class EmailErrorHandling
{
    public static Hashtable emailErrorHT { get; private set; } = new Hashtable();
    public static Hashtable threadSafeEmailHT { get; private set; } = Hashtable.Synchronized(emailErrorHT);

    public static async Task newResetHashMidnight()
    {
        int waitTime = 3600000;//every hour

        Program.log.Info("Creating hash watcher thread..");
        await System.Threading.Tasks.Task.Run(() =>
        {
            while (true)
            {
                System.Threading.Thread.Sleep(waitTime);//wait to scan again so that we don't destroy the server
                try
                {
                    if (DateTime.Now.Hour == 1)
                    {
                        Program.log.Info($"It has been: {DateTime.Now - new DateTime(2022, 6, 14, 14, 30, 00)} since Carl left.");
                        //trusting that this parse will work, if it doesn't it will get caught anyway
                        if (bool.Parse(FileManagement.getVariable(FileManagement.hashTableVar)))//this is me just dumping the hash to have a look at the errors for the day
                        {
                            Program.log.Info("Dumping email error hash table...");
                            lock (threadSafeEmailHT.SyncRoot)//lock writes in other threads so enumerator is valid if another threads writes
                            {
                                foreach (string key in threadSafeEmailHT.Keys)
                                {
                                    Program.log.Info("Email ID: " + key + " Error count: " + threadSafeEmailHT[key]);
                                }
                            }
                        }
                        Program.log.Info("Resetting hash...");
                        resetHash();//reset the hash at 1 am so that we don't have an inflated hash table with all of yesterday's errors
                    }
                }
                catch (Exception ex) 
                {
                    Program.log.Error("Error resetting hash: " + ex.Message);
                }
            }
        });
    }

    static public int addEmailItem(string id)
    {
        if (threadSafeEmailHT[id] == null)
        {
            threadSafeEmailHT.Add(id, 1);
            return 1;
        }
        else
        {
            threadSafeEmailHT[id] = int.Parse(threadSafeEmailHT[id].ToString()) + 1;
            return int.Parse(threadSafeEmailHT[id].ToString());
        }
    }

    static public void resetEmail(string id)
    {
        if (threadSafeEmailHT[id] != null)
        {
            threadSafeEmailHT[id] = 1;
        }
    }

    static public int? getCount(string id)
    {
        if (threadSafeEmailHT[id] != null)
        {
            return int.Parse(threadSafeEmailHT[id].ToString());
        }
        else
        {
            return null;
        }
    }

    static public void resetHash()
    {
        emailErrorHT = new Hashtable();
        threadSafeEmailHT = Hashtable.Synchronized(emailErrorHT);//make it thread safe
    }

    static public string getHashDump()
    {
        string dump = "Hash dump disabled.";
        if (bool.TryParse(FileManagement.getVariable(FileManagement.hashTableVar), out bool result))
        {
            if (result)
            {
                lock (threadSafeEmailHT.SyncRoot)//lock writes in other threads so enumerator is valid if another threads writes
                {
                    foreach (string key in threadSafeEmailHT.Keys)
                    {
                        dump += "Email ID: " + key + " Error count: " + threadSafeEmailHT[key] + "\r\n";
                    }
                }
            }
        }
        return dump;
    }
}