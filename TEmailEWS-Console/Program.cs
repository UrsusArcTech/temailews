﻿using System;
using System.Collections.Generic;
using System.Net;
using log4net;
[assembly: log4net.Config.XmlConfigurator]

class Program
{
    public static readonly ILog log = LogManager.GetLogger(typeof(Program));
    static List<EmailConnection> emailConnections = new List<EmailConnection>();

    static string command = "";
    static int noOfThreads = 0;
    static void Main(string[] args)
    {
        try
        {
            Program.log.Info("Starting TEmail " + EmailHandling.version);
            bool run = true;
            emailConnections = FileManagement.getEmails();
            FileManagement.getSettings();
            //NetworkMessage nwMsg = new NetworkMessage();
            //nwMsg.recieve();


            if (bool.TryParse(FileManagement.getVariable(FileManagement.sslCertVar), out bool result))
            {
                if (result)
                {
                    Program.log.Info("Turning off SSL checks...");
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; }; //turn ssl checks off if we need to
                }
            }

            EmailErrorHandling.newResetHashMidnight();

            foreach (var eConn in emailConnections)
            {
                noOfThreads++;
                Console.WriteLine("INFO: Creating procesing thread for: " + eConn.comEmail + " which forwards to: " + eConn.caEmail);
                log.Info("INFO: Creating procesing thread for: " + eConn.comEmail + " which forwards to: " + eConn.caEmail);
                EmailHandling.newMailThreadAsync(eConn.comEmail, eConn.caEmail, eConn.comPassword);
            }

            Console.WriteLine("MESSAGE: " + noOfThreads + " threads created succefully.");
            Console.WriteLine("MESSAGE: Check the log (TemailEWS.log) for status.");
            Console.WriteLine("----");
            Console.WriteLine("----");
            Console.WriteLine("Type help for list of settings");
            Console.WriteLine("----");

            while (run)
            {
                command = Console.ReadLine();
                if (command == "settings")
                {
                    Console.WriteLine("----");
                    foreach (var settingVariable in FileManagement.variableManagement.settingVariables)
                    {
                        Console.WriteLine(settingVariable.variable + settingVariable.setting);
                    }
                    Console.WriteLine("----");
                }
                else if(command == "help")
                {
                    Console.WriteLine("----");
                    Console.WriteLine("settings");
                    Console.WriteLine("emailconnections");
                    Console.WriteLine("dumphash");
                    Console.WriteLine("----");
                }else if(command == "emailconnections")
                {
                    Console.WriteLine("----");
                    foreach(var emailConn in emailConnections)
                    {
                        Console.WriteLine(emailConn.comEmail + " forwards to: " + emailConn.caEmail);
                    }
                    Console.WriteLine("----");
                }else if(command == "dumphash")
                {
                    lock (EmailErrorHandling.threadSafeEmailHT.SyncRoot)//lock writes in other threads so enumerator is valid if another thread writes
                    {
                        foreach (string key in EmailErrorHandling.threadSafeEmailHT.Keys)
                        {
                            log.Info("Email ID: " + key + " Error count: " + EmailErrorHandling.threadSafeEmailHT[key]);
                        }
                    }
                }
            }
        }
        catch (System.Exception e)
        {
            Console.WriteLine("ERROR: Something went wrong creating threads: " + e.Message);
        }

    }


}


